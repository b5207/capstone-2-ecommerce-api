//[SECTION A.] Dependencies & Modules.
	const jwt = require('jsonwebtoken');
	const dotenv = require('dotenv'); 


//[SECTION B.] Environment Variables Setup
	dotenv.config();
	const mySecret = process.env.SECRET;


//[SECTION C.] Token Creation
	module.exports.createAccessToken = (user) => {
		console.log(user);

		const data = {
			id: user._id,
			phoneNumber: user.phoneNumber,
			email: user.email,
			isAdmin: user.isAdmin
		}; //end of const data

		return jwt.sign(data,mySecret,{});

	}; //end of module.exports.createAccessToken


//[SECTION D.] Token Verification
	module.exports.verify = (req, res, next) => {
		console.log(req.headers.authorization);
		let token = req.headers.authorization;

		if(typeof token === "undefined"){
			return res.send({auth: "Failed. No token verified."});
		} else {
			console.log(token);
			token = token.slice(7, token.length);

			jwt.verify(token, mySecret, function(err, decodedToken){
				
				if(err){
					return res.send({
						auth: "Failed",
						message: err.message
					}); //end of return statement
				} else {
					console.log(decodedToken) 
					req.user = decodedToken; 

					next(); //middleware function	
				}; 

			}); //end of jwt.verify
		}; //end of parent if-else statement
	};// end of module.exports.verify


//[SECTION E.] Verification of An Admin
	module.exports.verifyAdmin = (req, res, next) => {
	
		if(req.user.isAdmin){
			next();
		} else {
			return res.send({
				auth: "Failed",
				message: "Action Forbidden."
			});//end of return
		};//end of if-else

	};//end of module.exports.verifyAdmin
