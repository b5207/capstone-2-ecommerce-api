//[SECTION A.] Dependencies and Modules
	const User = require('../models/User'); 
	const bcrypt = require('bcrypt'); 
	const dotenv = require('dotenv'); 
	const auth = require('../userAuth.js');

//[SECTION B.] Environment Variables Setup
	dotenv.config();
	const sodium = parseInt(process.env.SALT);


//[SECTION I.] FUNCTIONALITIES <CREATE>
	//1. Register New Account
	module.exports.register = (userData) => {
		let fullName = userData.fullName;
		let phoneNumber = userData.phoneNumber;
		let email = userData.email;
		let pass = userData.password;

		let newUser = new User({
			fullName: fullName,
			phoneNumber: phoneNumber,
			email: email,
			password: bcrypt.hashSync(pass, sodium)
		});//end of let newUser

		return newUser.save().then((fulfilled, rejected) => {
			if (fulfilled) {
				return fulfilled;
			} else {
				return 'Failed to register new account.';
			};//end of if-else

		}); //end of return newUser

	};//end of module.exports.register


//[SECTION II.] FUNCTIONALITIES <RETRIEVE>
	/*STEPS:
	1. Find the document in the database using the user's ID.
	2. Reassign the password of the returned document to an empty string.
	3. Return the result back to the client.*/
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			//change the value of the user's password to an empty string.
			result.password = '';
			return result;
		}); //end of return statement
	}; //end of module.export.getProfile


//[SECTION III.] FUNCTIONALITIES <UPDATE>


//[SECTION IV.] FUNCTIONALITIES <DELETE>


//[SECTION V.] User Authentication
	/*
	STEPS:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the db.
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
	*/
	module.exports.loginUser = (data) => {
		//findOne method returns the 1st record in the collection that matches the search criteria.
		return User.find({$or: [{phoneNumber: data.phoneNumber}, {email: data.email}]}).then(result => {
			//if User *exists*
			if(result !== null) { 
				//'compareSync' method from the bcrypt to be used in comparing the non-encrypted password from the login and the database password.
				const isPasswordCorrect = bcrypt.compareSync(data.password, result[0].password)

				//if the password matched, return token
				if(isPasswordCorrect){
					return { accessToken: auth.createAccessToken(result[0].toObject())}
				} else {
					//password does not match
					return false;
				}
				
			} else {
				//else User *does not exist*
				return false;
			}; //end of parent if-else

		}); //end of return statement
	}; //end of module.exports.loginUser


//[SECTION VI.] STRETCH GOAL - Set User As Admin
	module.exports.setUserAsAdmin = (userId) => {
		let changedProperty = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(userId, changedProperty).then((updatedUserStatus, error) => {
			if(error){
				return false;
			} else {
				return changedProperty;
			}
		}).catch(error => error)
	}; //end of module.exports.setUserAsAdmin


//[SECTION VII.] ADDED FUNCTIONALITY <RETRIEVE ALL USERS>
	module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
			return result;
		})
	};//end of module.exports.getAllUsers