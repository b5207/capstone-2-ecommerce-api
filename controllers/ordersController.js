//[SECTION A.] Dependencies and Modules
	const Order = require('../models/Order');
	const Product = require('../models/Product');
	const User = require('../models/User'); 
	const auth = require('../userAuth.js');

//[SECTION B.] FUNCTIONALITY <Create New Order>
	module.exports.createOrder = (products, totalAmount, user) => {
		
		
		if(user.isAdmin) {
			return {Warning: "You are creating an order using your Admin Access. Please use your personal account."}
		}

		let newOrder = new Order()

		newOrder.userId = user.id 
		newOrder.totalAmount = totalAmount

		products.forEach(product => {
			newOrder.products.push({
				productId: product.productId,
				prodName: product.name,
				quantity: product.quantity,
				price: product.price
			})
		})

		return newOrder.save().then((result, error) => {
			if(error){
				return error 
			}

			return result
		}).catch(error => {
			return error
		})
	
	}; //end of module.exports.createOrder


//[SECTION C.] STRETCH GOAL - <Retrieve AUTHENTICATED User's Orders>
	module.exports.getUserOrder = (req, res) => {
		return Order.find({'userId': req.user.id}).then(result =>{
			res.send(result);

		}).catch(error => error)
	}; //end of module.exports.getUserOrder


//[SECTION D.] STRETCH GOAL - <Retrieve ALL Orders [Admin only]>
	module.exports.getAllOrders = async () =>{
    return await Order.find().then(result => {
    	return result;
    }).catch(error => error.message)

	}

	/*module.exports.getAllOrders = () => {
		return Order.find({}).then(result => {
			return result;
			console.log(result);
		}).catch(error => error.message)
	}; //end of module.exports.getAllOrders*/


//[SECTION E.] <Remove Order By User>
	module.exports.removeProduct = (productId) => {
		return Order.findByIdAndRemove(productId).then((fulfilled, rejected) => {
			if(fulfilled){
				return 'The product has been successfully removed.';
			} else {
				return 'Failed to remove product.';
			}
		})
	}