//[SECTION A.] Dependencies and Modules
	const Product = require('../models/Product'); 

 
//[SECTION B.] POST <Create a New Product>
	module.exports.addProduct = (productData, prodFile, url) => {

		let newProduct = new Product ({
			name: productData.name,
			description: productData.description,
			price: productData.price,
			image: url + "/images/" +  prodFile.filename
		});

		return newProduct.save().then((fulfilled, error) => {
			if (error) {
				return false;
			} else {
				return fulfilled;
			};//end of if-else

		}).catch(error => error)
	};//end of module.exports.addProduct


//[SECTION C.] GET <Retrieve All Products by Admin>
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		})
	}; //end of module.exports.getAllProducts


//[SECTION D.] GET <Retrieve All ACTIVE Products>
	//1. Retrieve all products with the property isActive: true
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		}).catch(error => error)
	}; //end of module.exports.getAllActive


//[SECTION E.] GET <Retrieve a Specific Product>
	//1. Retrieve the products that matches the course ID provided from the URL.
	module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams).then(result => {
			return result;
		}).catch(error => error)
	}; //end of module.exports.getProduct


//[SECTION F.] PUT <Update A Product>
	/*STEPS:
		1. Create a variable "updateProduct" that will contain info from the req.body
		2. Find and update the product using the productId retrieved from the req.params and the variable "updatedProduct" containing info from req.body.
	*/
	module.exports.updateProduct = (productId, data, reqFile, url) => {
		//Specify the fields/properties of the document to be updated.
		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price,
			image: url + "/images/" + reqFile.filename
		}
		//findByIdAndUpdate(documentId, updatesToBeApplied)
		return Product.findByIdAndUpdate(productId, updatedProduct).then((success, error) => {
			if(error){
				return false;
			} else{
				return updatedProduct;
			}
		}).catch(error => error)
	};//end of module.exports.updateCourse


//[SECTION G.] PUT <Archiving A Product>
	//1. Update the status of "isActive" into false which will no longer be displayed in the client whenever all active products are retrieved.
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive: false
		};
		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
			if(error){
				return false;
			} else {
				return updateActiveField;
			}
		}).catch(error => error)
	}; //end of module.exports.archiveProduct


//[SECTION H.] PUT <Activating A Course>
	module.exports.activateProduct = (productId) => {
		let updateActiveField = {
			isActive: true
		};
		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
			if(error){
				return false;
			} else {
				return updateActiveField;
			}
		}).catch(error => error)
	}; //end of module.exports.activateCourse