// //[SECTION A.] Dependencies and Modules
	// const express = require ('express');
	// const mongoose = require('mongoose');
	// const cors = require('cors');
	// const dotenv = require('dotenv');
	// const userRouter = require('./routes/usersRoute');
	// const productRouter = require('./routes/productsRoute');
	// const orderRouter = require('./routes/ordersRoute');

// //[SECTION B.] Environment Setup
// 	dotenv.config();
// 	let dbCredentials = process.env.CREDENTIALS;
// 	const port = process.env.PORT;

//[SECTION C.] Server Setup
	// const app = express();
	// app.use(cors());
	// app.use(express.json()); //middleware
	// app.use(express.static(__dirname))

// //[SECTION D.] Database Connection
// 	mongoose.connect(dbCredentials);
// 	const connectStatus = mongoose.connection;
// 	connectStatus.once('open', () => console.log('Database is now connected!'));
	
// //[SECTION E.] Backend Routes
// 	app.use('/users', userRouter);
// 	app.use('/products', productRouter);
// 	app.use('/orders', orderRouter);

// //[SECTION F.] Server Gateway Response
	// app.get('/', (req, res) => {
	// 	res.send(`Welcome to the API of Emporium Shopping Website!`);
	// }); //end of app.get
	// app.listen(port, () => {
	// 	console.log(`API is hosted on port ${port}.`);
	// }); //end of app.listen

//[SECTION A.] Dependencies and Modules
const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const userRouter = require('./routes/usersRoute');
const productRouter = require('./routes/productsRoute');
const orderRouter = require('./routes/ordersRoute');

//[SECTION B.] Environment Setup
dotenv.config();
let dbCredentials = process.env.CREDENTIALS;
const port = process.env.PORT;

//[SECTION C.] Server Setup
const app = express();
app.use(cors());
app.use(express.json()); //middleware
app.use(express.static(__dirname));

//[SECTION D.] Database Connection
const { MongoClient } = require('mongodb');

const client = new MongoClient(dbCredentials, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function connectToDatabase() {
  try {
    await client.connect();
    console.log("Connected to MongoDB successfully!");
    return client.db("Project 0"); // Replace "yourDatabaseName" with your actual database name
  } catch (error) {
    console.error("Failed to connect to MongoDB:", error);
    throw error;
  }
}

// Call this function to establish the database connection and get the database object
async function getDatabase() {
  if (!client.isConnected()) {
    await connectToDatabase();
  }
  return client.db("Project 0"); // Replace "yourDatabaseName" with your actual database name
}

//[SECTION E.] Backend Routes
app.use(async (req, res, next) => {
  try {
    req.db = await getDatabase();
    next();
  } catch (error) {
    console.error('Failed to get database connection:', error);
    res.status(500).send('Internal server error');
  }
});

app.use('/users', userRouter);
app.use('/products', productRouter);
app.use('/orders', orderRouter);

//[SECTION F.] Server Gateway Response
app.get('/', (req, res) => {
  res.send(`Welcome to the API of Emporium Shopping Website!`);
});

// Start the server
app.listen(port, () => {
  console.log(`API is hosted on port ${port}.`);
});
  



