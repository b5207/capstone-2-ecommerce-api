 //[SECTION A.] Dependencies & Modules
	const exp = require('express');
	const route = exp.Router();
	const UserController = require('../controllers/usersController');
	const auth = require('../userAuth.js');

	//Destructure the actual function that we need to use.
	const {verify, verifyAdmin} = auth;


//[SECTION B.] Route - <POST> User's Registration Info 
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body;
		//invoke the controller function you wish to execute. Variable name of the controller is in [Section A.]
		UserController.register(userData).then(outcome => {
			res.send(outcome);
		});
	}); //end of route.post

 
//[SECTION C.] Route - <POST> User Authentication (login)
	route.post('/login', (req, res) => {
		UserController.loginUser(req.body).then(result => res.send(result));
	}); //end of route.post


//[SECTION D.] Route - <GET> User's Details
	route.get('/details', auth.verify, (req, res) => {
		UserController.getProfile(req.user.id).then(result => res.send(result));
	}); //end of route.get


//[SECTION E.] STRETCH GOAL - <PUT> Set User As Admin [Admin Only]
	route.put('/:userId/make-user-admin', verify, verifyAdmin, (req, res) => {
		UserController.setUserAsAdmin(req.params.userId).then(result => res.send(result));
	}); //end of route.put


//[SECTION F.] Routes - PUT


//[SECTION G.] Routes - DEL


//[SECTION H.] ADDED FEATURE - <GET> Retrieve All Users [Admin Only]
	route.get('/allUsers', verify, verifyAdmin, (req, res) => {
		UserController.getAllUsers().then(result => res.send(result));
	}); //end of route.get


//[SECTION H.] Expose Route System
	module.exports = route;