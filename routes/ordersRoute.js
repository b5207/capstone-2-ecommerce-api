//[SECTION A.] Dependencies & Modules
	const express = require('express');
	const route = express.Router();
	const OrderController = require('../controllers/ordersController');
	const auth = require('../userAuth');
	//Destructure the actual function that we need to use.
	const {verify, verifyAdmin} = auth;
	 

//[SECTION B.] POST <Create New Order> 
	route.post('/createOrder', verify, (req, res) => {
		OrderController.createOrder(req.body.products, req.body.totalAmount, req.user).then(result => {
			res.send(result)
		})
	})
	//end of route.post

 
//[SECTION C.] STRETCH GOAL: GET <Retrieve AUTHENTICATED User's Orders>
	route.get('/userOrders', verify, OrderController.getUserOrder);
	 //end of route.get


//[SECTION D.] STRETCH GOAL: GET <Retrieve ALL Orders [Admin only]>
	route.get('/allOrders', verify, verifyAdmin, (req, res) => {
		OrderController.getAllOrders().then(result => res.send(result));
	}); //end of route.get


//[SECTION E.] Routes - PUT

//[SECTION F.] Routes - DEL
	route.delete('/:removeProduct', verify, (req, res) => {
		console.log(req.params.removeProduct);
		let prodDelId = req.params.removeProduct;
		OrderController.removeProduct(prodDelId).then(result => res.send(result));
	}); //end of route.

//[SECTION G.] Expose Route System
	module.exports = route;