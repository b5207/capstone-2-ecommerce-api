//[SECTION A.] Dependencies & Modules
	const express = require('express');
	const route = express.Router();
	//Use Pascal case for controller names so that you will remember it's important.
	const ProductController = require('../controllers/productsController');
	const auth = require('../userAuth');

	//MULTER
	const multer = require('multer');
	const path = require('path');
	
	//Destructure the actual function that we need to use.
	const {verify, verifyAdmin} = auth;


	const storage = multer.diskStorage({
	    destination: function (req, file, callback) {
	        callback(null, path.join(__dirname, '../images/'))
	    },
	    filename: function(request, file, callback){
	        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
	    }
	})

	const upload = multer({
	    storage: storage,
	    fileFilter: (req, file, cb) => {
	        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
	            cb(null, true);
	        } else {
	            cb(null, false);
	            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
	        }
	    }
	})



//[SECTION B.] POST <Create New Product>
	route.post('/create', verify, verifyAdmin, upload.single('image'), (req, res) => {
		const url = req.protocol + '://' + req.get('host')
		ProductController.addProduct(req.body, req.file, url).then(result => res.send(result))
	});//end of route.post


//[SECTION C.] GET <Retrieve All Products by Admin>
	route.get('/all', (req, res) => {
		ProductController.getAllProducts().then(result => res.send(result));
	});//end of route.get


//[SECTION D.] GET <Retrieve All ACTIVE Products>
	route.get('/active', (req, res) => {
		ProductController.getAllActive().then(result => res.send(result));
	});


//[SECTION E.] GET <Retrieve A Specific Product>
	//req.params
	route.get('/:productId', (req, res) => {
		console.log(req.params.productId)
		//we can retrieve the course ID by accessing the request's "params" property that contains all the parameters provided via the URL.
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	});//end of route.get


//[SECTION F.] PUT <Updating A Product>
	//what your value is in the wildcard ('/:') should be the same in the req.params."wildcard"
	route.put('/:productId', verify, verifyAdmin, upload.single('image'), (req, res) => {
		const url = req.protocol + '://' + req.get('host')
		ProductController.updateProduct(req.params.productId, req.body, req.file, url).then(result => res.send(result));
	});//end of route.put


//[SECTION G.] PUT <Archiving A Product>
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	});//end of route.put


//[SECTION H.] PUT <Activate A Course>
	route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
		ProductController.activateProduct(req.params.productId).then(result => res.send(result));
	});//end of route.put


//[SECTION I.] Expose Route System
	module.exports = route;