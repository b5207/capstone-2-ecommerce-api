//[SECTION A.] Modules and Dependencies
	const mongoose = require('mongoose');


//[SECTION B.] Schema/Blueprint
	const orderSchema = new mongoose.Schema({
		userId: {	
			type: String,
      		ref: 'User',
       		required: [true, 'User ID is Required']
		},
		products: [
			{
				productId: {
					type: mongoose.Schema.Types.ObjectId, 
            		ref: 'Product',
            		required: [true, 'Product ID is Required']
				},
				prodName: {
					type: String,
					required: [true, 'Product Name is required for this order.']
				},
				quantity: {
					type: Number,
					default: 1
				},
				price: {
					type: Number,
					required: [true, 'Product Price is required for this order.'] 
				}
			}//closing curly brace
		], //array closing bracket
		totalAmount: {
			type: Number,
			required: [true, 'Total Amount is required for checkout.']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
		
	}); //end of mongoose.Schema

//[SECTION C.] Model
	module.exports = mongoose.model('Order', orderSchema);