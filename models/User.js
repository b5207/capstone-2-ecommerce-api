//[SECTION A.] Modules and Dependencies
	const mongoose = require('mongoose');


//[SECTION B.] Schema/Blueprint
	const userSchema = new mongoose.Schema({
		
		fullName: {
			type: String,
			required: [true, 'Full Name is required.']
		},
		phoneNumber: {
			type: String
		},
		email: {
			type: String,
			required: [true, 'Email is required.']
		},
		password: {
			type: String,
			required: [true, 'Password is required.']
		},
		isAdmin: {
			type: Boolean,
			default: false
		}
	}); //end of mongoose.Schema


//[SECTION C.] Model
	module.exports = mongoose.model('User', userSchema);