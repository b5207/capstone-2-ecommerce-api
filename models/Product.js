//[SECTION A.] Dependencies and Modules
	const mongoose = require('mongoose');


//[SECTION B.] Schema/Blueprint
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Product Name is required.']
		},
		description: {
			type: String,
			required: [true, 'Product Description is required.']
		},
		price: {
			type: Number,
			required: [true, 'Product Price is required.']
		},
		image: {
			type: String,
			required: [true, 'Product image is required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}

	});//end of mongoose.Schema


//[SECTION C.] Model
	module.exports = mongoose.model('Product', productSchema);